/*array personajes*/
let influencer = [
	{
		idinfluencer:1,
		imginfluencer:"assets/images/ranking/fem/normal/danyancat.png",
		imginfluencer_hover:"assets/images/ranking/fem/active/",
		nombrepersonaje:"Danyancat",
		descpersonaje:"Fanática de los videojuegos, el anime y el cosplay. Actualmente hace streams en Facebook Gaming."
	},
	{
		idinfluencer:2,
		imginfluencer:"assets/images/ranking/fem/normal/lizeth.png",
		imginfluencer_hover:"assets/images/ranking/fem/active/lizeth.png",
		nombrepersonaje:"Lizeth",
		descpersonaje:"Originaria de Guadalajara, es youtuber y streamer de Booyah! Latam. Le gusta Pokémon."
	},
	{
		idinfluencer:3,
		imginfluencer:"assets/images/ranking/fem/normal/milkaplay.png",
		imginfluencer_hover:"assets/images/ranking/fem/active/milkaplay.png",
		nombrepersonaje:"Milkaply",
		descpersonaje:"Creadora de contenido en Estorm Gaming y streamer en Booyah! Latam."
	},
	{
		idinfluencer:4,
		imginfluencer:"assets/images/ranking/fem/normal/princessx.png",
		imginfluencer_hover:"assets/images/ranking/fem/active/princessx.png",
		nombrepersonaje:"Princessx",
		descpersonaje:"Youtuber y streamer en Booyah! Latam. Su color favorito es el azul."
	},
	{
		idinfluencer:5,
		imginfluencer:"assets/images/ranking/fem/normal/lucy.png",
		imginfluencer_hover:"assets/images/ranking/fem/active/lucy.png",
		nombrepersonaje:"Lucy",
		descpersonaje:"Originaria de Tucumán, hace streams en Twitch y adora a su mascota, un bulldog inglés."
	},
	{
		idinfluencer:6,
		imginfluencer:"assets/images/ranking/male/normal/antronix.png",
		imginfluencer_hover:"assets/images/ranking/male/active/antronix.png",
		nombrepersonaje:"Antronix",
		descpersonaje:"Originario de Monterrey, actualmente es streamer en Nimo TV y líder del clan GameoverYT."
	},
	{
		idinfluencer:7,
		imginfluencer:"assets/images/ranking/male/normal/boomsniper.png",
		imginfluencer_hover:"assets/images/ranking/male/active/boomsniper.png",
		nombrepersonaje:"Boomsniper",
		descpersonaje:"Misterioso streamer que nunca muestra su rostro, hace transmisiones en Nimo TV. Es considerado uno de los mejores jugadores de Free Fire."
	},	
	{
		idinfluencer:8,
		imginfluencer:"assets/images/ranking/male/normal/luay.png",
		imginfluencer_hover:"assets/images/ranking/male/active/luay.png",
		nombrepersonaje:"Luay",
		descpersonaje:"Nació en Trujillo y actualmente vive en Tucumán con su novia, Lucy. Comenzó su carrera en Youtube y actualmente es streamer de NimoTV."
	},	
	{
		idinfluencer:9,
		imginfluencer:"assets/images/ranking/male/normal/thedonato.png",
		imginfluencer_hover:"assets/images/ranking/male/active/thedonato.png",
		nombrepersonaje:"The donato",
		descpersonaje:"Uno de los más grandes representantes de Free Fire en el mundo y el youtuber de Venezuela con más suscripciones. Ganó los Nickelodeon Mexico Kids Choice Awards 2020 en la categoría de jugador favorito."
	},
	{
		idinfluencer:10,
		imginfluencer:"assets/images/ranking/male/normal/yair17.png",
		imginfluencer_hover:"assets/images/ranking/male/active/yair17.png",
		nombrepersonaje:"Yair17",
		descpersonaje:"Lorem ipsum dolor sit amet, consectetur adipisicing, elit. Cum blanditiis voluptatem in amet eaque quae dolorum sunt, pariatur excepturi quidem praesentium necessitatibus vel, aliquam! Soluta recusandae, laborum animi aliquam officia?"
	},	
]
$(document).ready(function(){
   
   /*carga slide*/
		$.each(influencer, function( key, element) {
		  // console.log(key, element)
		  let current = key + 1
		  $('.i_mujer[data-id='+current+']').find('img.inf_img').attr('src', element.imginfluencer)
		  $('.i_mujer[data-id='+current+']').find('img.p_hover').attr('src', element.imginfluencer_hover)
		  $('.i_mujer[data-id='+current+']').find('.nombre').text(element.nombrepersonaje)
		  $('.i_hombre[data-id='+current+']').find('img.inf_img').attr('src', element.imginfluencer)
		  $('.i_hombre[data-id='+current+']').find('img.p_hover').attr('src', element.imginfluencer_hover)
		  $('.i_hombre[data-id='+current+']').find('.nombre').text(element.nombrepersonaje)
		}); 
	/*fin carga slide*/
		
    /*carga array*/
     	$(".i_mujer").click(function(){
	     	var id = $(this).data('id')
	     	influencer.forEach(function(element){
	     		if(element.idinfluencer === id){
	     			$('.img_inf_m img').attr('src', element.imginfluencer)
	     			$('.nombre_influ').text(element.nombrepersonaje)
	     			$('.infle_desc').text(element.descpersonaje)
	     			$('.w_mujer').fadeIn();
	     			$(".personaje__view").css('display','flex');
	     		}
	     	})
	 	});

	 	$(".i_hombre").click(function(){
	     	var id = $(this).data('id')
	     	influencer.forEach(function(element){
	     		if(element.idinfluencer === id){
	     			$('.img_inf_h img').attr('src', element.imginfluencer)
	     			$('.nombre_influ').text(element.nombrepersonaje)
	     			$('.infle_desc').text(element.descpersonaje)
	     			$('.w_hombre').fadeIn();
	     			$(".personaje__view").css('display','flex');
	     		}
	     	})
	 	});
	 /*fin carga vista*/
	 	
	 /*cerrar*/
	$(".personaje__view .cerrar").click(function(){
	   $(".personaje__view").fadeOut();
  	});
  	/*mostrar ranking*/
  	$(".voto_p").click(function(){
	   $(".personaje__view").hide();
	   $(".personaje__view_random").hide();
	   $(".contenedor_personajes").hide();
	   $(".ranking_personajes").show();
	   $(".ranking_personajes").css('display','flex');
  	});

  });