

$(document).ready(function(){
  /*Log-in*/
  $(".start").click(function(){
    $(".bienvenida").hide();
    $(".acceso").fadeIn(1000);
  });
  /*mapa*/
  $(".h_option").click(function(){
    $(".opciones_view").fadeIn();
    $(".opciones_view").css('display','flex');
  });
  $(".opciones_view .cerrar").click(function(){
    $(".opciones_view").fadeOut();
  });
  /*Reto 2*/
  $(".i_mujer").click(function(){
    $(".w_mujer").fadeIn(1000);
  });
  $(".w_mujer .cerrar").click(function(){
    $(".w_mujer").fadeOut(1000);
  });
  $(".s_mujer").click(function(){
    $(".w_mujer").hide();
    $(".mujer").hide();
    $(".r_mujer").fadeIn();
    $(".r_mujer").css('display' , 'flex');
  });
  $(".r_mujer .siguiente_voto").click(function(){
    $(".r_mujer").hide();
    $(".hombre").fadeIn();
    // $(".hombre").css('display' , 'flex');
  });
  /*hombre*/
  $(".i_hombre").click(function(){
    $(".w_hombre").fadeIn(1000);
  });
  $(".w_hombre .cerrar").click(function(){
    $(".w_hombre").fadeOut(1000);
  });
  $(".s_hombre").click(function(){
    $(".w_hombre").hide();
    $(".hombre").hide();
    $(".r_hombre").fadeIn();
    $(".r_hombre").css('display' , 'flex');
  });
   $(".thanks").click(function(){
    $(".contenedor_influencer").hide();
    $(".thanks__influencer").fadeIn();
    $(".thanks__influencer").css('display','flex');
  });

   /*Quiz*/
  $(".error").click(function(){
    $(".incorrecta").fadeIn();
    $(".incorrecta").css('display','flex');
  });
  $(".again").click(function(){
    $(".incorrecta").fadeOut();
  });
  $(".true_o").click(function(){
    $(".p_uno").hide();
    $(".p_dos").fadeIn(1100);
  });
  $(".true_d").click(function(){
    $(".p_dos").hide();
    $(".p_tres").fadeIn();
  });
  $(".true_t").click(function(){
    $(".p_tres").hide();
    $(".p_cuatro").fadeIn();
  });
  $(".true_c").click(function(){
    $(".p_cuatro").hide();
    $(".p_cinco").fadeIn();
  });
  $(".true").click(function(){
    $(".contenedor_quiz").hide();
    $(".fin__quiz").fadeIn();
    $(".fin__quiz").css('display','flex');
  });
  /*reto 4*/
   $(".r_video").click(function(){
    $(".reto_cuatro").fadeIn();
    $(".reto_cuatro").css('display','flex');
  });
   $(".reto_cont .cerrar").click(function(){
    $(".reto_cuatro").fadeOut();
    // $(".reto_cuatro").css('display','flex');
  });

   /*retos rankings*/
   $('.pass_reto_uno').click(function() {
      window.location.href='../../ranking-personajes.html';
  });
   $('.pass_reto_dos').click(function() {
      window.location.href='../../ranking-influencer.html';
  });
});
/*video*/
document.getElementById('vfree').addEventListener('ended',myHandler,false); 
 function myHandler(e) { 
     $(".contenedor_video").hide();
      $(".thank_video").fadeIn();
      $(".thank_video").css('display','flex');
} 

