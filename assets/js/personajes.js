/*array personajes*/
let arraypersonajes = [
	{
		idpersonaje:1,
		splash:'assets/images/splash-personajes/luqueta.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/luqueta.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/luqueta.png",
		nombrepersonaje:"Luqueta",
		descpersonaje:"Luqueta era un estudiante de intercambio al extranjero. Se mudó por el trabajo de su padre pero Luqueta había visto la vida de su padre y no quería nada de eso. Siempre fue un niño atlético. Cuando se mudó comenzó a jugar fútbol para la escuela y se convirtió en una estrella."
	},
	{
		idpersonaje:2,
		splash:'assets/images/splash-personajes/clu.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/clu.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/clu.png",
		nombrepersonaje:"Clu",
		descpersonaje:"Clu creció en el suburbio de una gran ciudad. Sus padres eran ricos y ella tuvo una infancia muy amorosa. Cuando era una adolescente, su padre fue reclutado para la guerra y él nunca regresó a casa. Nunca fue encontrado. Clu se convirtió en detective privado con la esperanza de poder descubrir detalles sobre la desaparición de su padre."
	},
	{
		idpersonaje:3,
		splash:'assets/images/splash-personajes/a124.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/a124.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/a124.png",
		nombrepersonaje:"a124",
		descpersonaje:"A124 es un robot hecho con la última tecnología. Ella tiene dos formas de comportarse: como una chica normal de 18 años o como un arma mortal."
	},
	{
		idpersonaje:4,
		splash:'assets/images/splash-personajes/alvaro.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/alvaro.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/alvaro.png",
		nombrepersonaje:"Alvaro",
		descpersonaje:"Alvaro creció en una familia militar. Fue reclutado para las fuerzas armadas a los 14 años cuando descubrieron su gusto por los explosivos. Hacía sus propios explosivos y volaba cosas en el bosque detrás de la casa de su familia. Alvaro es visto por la gente común como poco confiable y loco."
	},
	{
		idpersonaje:5,
		splash:'assets/images/splash-personajes/nikita-ford-andrew.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/andrew.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/andrew.png",
		nombrepersonaje:"Andrew",
		descpersonaje:"Andrew era oficial de policía. Con su fuerte sentido de justicia, pasaba la mayoría de su tiempo persiguiendo criminales. Buscar la verdad detrás de todo era su llamado del deber."
	},
	{
		idpersonaje:6,
		splash:'assets/images/splash-personajes/antonio.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/antonio.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/antonio.png",
		nombrepersonaje:"Antonio",
		descpersonaje:"Antonio, un reconocido mafioso. Él era un niño huérfano que creció como un mafioso. Creó su propia banda de mafia y ha puesto su vida en peligro varias veces para proteger a sus seres queridos."
	},
	{
		idpersonaje:7,
		splash:'assets/images/splash-personajes/caroline.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/caroline.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/caroline.png",
		nombrepersonaje:"Caroline",
		descpersonaje:"Caroline es una chica que creció en una familia muy adinerada. Siempre está acompañada de guardaespaldas a donde quiera que vaya. Sin duda la más popular del colegio. Su padre y sus amigos son lo que más atesora en la vida."
	},
	{
		idpersonaje:8,
		splash:'assets/images/splash-personajes/nikita-ford-andrew.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/ford.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/ford.png",
		nombrepersonaje:"Ford",
		descpersonaje:"Ford era un hombre ordinario de la armada pero lo que es importante son sus motivos y acciones. Ford era un hombre muy resistente. La única persona que puede hacer vacilar su determinación, era probablemente ella ..."
	},
	{
		idpersonaje:9,
		splash:'assets/images/splash-personajes/hayato.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/hayato.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/hayato.png",
		nombrepersonaje:"Hayato",
		descpersonaje:"Hayato, un chico de una legendaria familia Samurái. Siendo el único hijo de la familia, Hayato es el único que debe seguir con la tradición y maldición a su vez... Este samurái tiene un secreto que nadie conoce... Es por eso que Hayato desea que su vida termine lo antes posible."
	},
	{
		idpersonaje:10,
		splash:'assets/images/splash-personajes/joseph.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/joseph.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/joseph.png",
		nombrepersonaje:"Joseph",
		descpersonaje:"Joseph es conocido por ser un científico así como un casanova.<br>Fue el fundador de una empresa tecnológica realmente exitosa. También fue un estudiante sobresaliente en la academia militar."
	},
	{
		idpersonaje:11,
		splash:'assets/images/splash-personajes/kapella.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/kapella.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/kapella.png",
		nombrepersonaje:"Kapella",
		descpersonaje:"Kapella creció en una familia que reconocía su talento para cantar y que deseaba que fuera famosa. La fama ayudó a su familia a ganar respeto en la sociedad pero no se había dado cuenta de que su familia la estaba usando para sus propios fines."
	},
	{
		idpersonaje:12,
		splash:'assets/images/splash-personajes/kelly.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/kelly.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/kelly.png",
		nombrepersonaje:"Kelly",
		descpersonaje:"Kelly era velocista de secundaria. También se conoce como Shimada Kirika. <br>A ella, le gusta mucho correr y siempre puede ser vista en la pista. Correr hacia adelante ha sido incorporado en su alma y su corazón. No importa el obstáculo, siempre avanzará."
	},
	{
		idpersonaje:13,
		splash:'assets/images/splash-personajes/kla.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/kla.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/kla.png",
		nombrepersonaje:"Kla",
		descpersonaje:"Kla es un gran artista marcial, su especialidad es el Muay Thai. <br>Hace unos años, desapareció misteriosamente y nadie lo pudo encontrar. <br>El Kla que reapareció frente a todos no es el mismo que el famoso Kla que todos conocían. Ahora es un vengador, y no tiene piedad con nadie."
	},
	{
		idpersonaje:14,
		splash:'assets/images/splash-personajes/laura.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/laura.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/laura.png",
		nombrepersonaje:"Laura",
		descpersonaje:"Laura es una agente especial excepcional. Ella es una gran tiradora desde que era pequeña. Su objetivo es traer la justicia al mundo."
	},
	{
		idpersonaje:15,
		splash:'assets/images/splash-personajes/maxim.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/maxim.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/maxim.png",
		nombrepersonaje:"Maxim",
		descpersonaje:"Maxim ama hacer stream de cuando comer. Con un gran apetito, nunca dice que no a la comida. Con alegría y disposición de nacimiento, Maxim es apreciado por sus cercanos."
	},
	{
		idpersonaje:16,
		splash:'assets/images/splash-personajes/miguel.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/miguel.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/miguel.png",
		nombrepersonaje:"Miguel",
		descpersonaje:"Miguel es un soldado muy bien balanceado de las Fuerzas Especiales. A él no le importa que tan difícil sea la misión, mientras pueda proveer de justicia. Sin embargo, luego de una operación fallida 6 meses atrás, se dio cuenta que fue traicionado por sus aliados de confianza."
	},
	{
		idpersonaje:17,
		splash:'assets/images/splash-personajes/misha.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/misha.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/misha.png",
		nombrepersonaje:"Misha",
		descpersonaje:"Misha es una talentosa conductora. Sus increíbles habilidades en el volante le han dado el título de Reina de las carreras en la comunidad. Siempre le gustan los nuevos retos, y subió rápidamente a la fama tras cada torneo competido."
	},
	{
		idpersonaje:18,
		splash:'assets/images/splash-personajes/moco.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/moco.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/moco.png",
		nombrepersonaje:"Moco",
		descpersonaje:"Moco, la leyenda del Mundo Cibernético. También es conocida como Gato Negro por su alta habilidad e inteligencia. Ella puede hackear cualquier PC sin que nadie lo note. Al tener la información que ella busca, desaparece como un fantasma."
	},
	{
		idpersonaje:19,
		splash:'assets/images/splash-personajes/nikita-ford-andrew.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/nikita.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/nikita.png",
		nombrepersonaje:"Nikita",
		descpersonaje:"Nikita es una guardaespaldas profesional. Su tarea más importante es proteger y escoltar a Carolyn, la hija del presidente de la organización Free Fire. Antes, Nikita era una tiradora de rifle de aire en la escuela, lo que le ayudó a pulir su agilidad y agudizar sus sentidos."
	},
	{
		idpersonaje:20,
		splash:'assets/images/splash-personajes/notora.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/notora.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/notora.png",
		nombrepersonaje:"Notora",
		descpersonaje:"Notora es una piloto de motos. Ella nació en una familia que era parte de una pandilla de motociclistas, pero odia una vida llena de asesinatos. Hubo un tiroteo entre su pandilla y otro rival, todo su equipo fue aniquilado y ella fue capturada. Debido a su actuación en la batalla, evitó la muerte y fue enviada a la isla Free Fire."
	},
	{
		idpersonaje:21,
		splash:'assets/images/splash-personajes/olivia.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/olivia.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/olivia.png",
		nombrepersonaje:"Olivia",
		descpersonaje:"Olivia era enfermera. No importa quien sea, amigo o enemigo, siempre entrega lo todo para cuidar a sus pacientes. Se dice que los pacientes resucitados por su toque sanador, obtendrá una fuerza inexplicable y confianza en la vida. Olivia es una compañera muy confiable en todas las situaciones."
	},
	{
		idpersonaje:22,
		splash:'assets/images/splash-personajes/paloma.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/paloma.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/paloma.png",
		nombrepersonaje:"Paloma",
		descpersonaje:"Paloma tiene un pasado difícil, es una reina de la belleza criada en los suburbios más pobres de su país, ella era la modelo a seguir de todas las chicas. Pero ahora, es el pináculo del mal, una traficante de armas que tiene el bajo mundo a sus pies."
	},
	{
		idpersonaje:23,
		splash:'assets/images/splash-personajes/rafael.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/rafael.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/rafael.png",
		nombrepersonaje:"Rafael",
		descpersonaje:"Rafael es un asesino mortal. Su objetivo es traer justicia al mundo."
	},
	{
		idpersonaje:24,
		splash:'assets/images/splash-personajes/shani.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/shani.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/shani.png",
		nombrepersonaje:"Shani",
		descpersonaje:"Shani es una ingeniera hecha a sí misma. Sus padres murieron en la explosión de un edificio cuando tenía 18 años y perdió todo lo que tenía. <br>Estuvo deambulando durante unos años haciendo trabajos extraños para ganarse la vida, cuando el dueño de un depósito de chatarra la acogió y le dio un lugar para llamar hogar. Ahí pudo trabajar con piezas basura e ingeniar a su gusto. "
	},
	{
		idpersonaje:25,
		splash:'assets/images/splash-personajes/steffie.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/steffi.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/steffi.png",
		nombrepersonaje:"Steffie",
		descpersonaje:"Steffie es una liberalista. Desde la infancia, ya había mostrado un sorprendente don para el arte. Después de la guerra, se convirtió en una famosa rebelde, dejando increíbles grafitis en la ciudad para reírse de la cobardía. Para devolver la libertad a este mundo, se unió a Cibernética, convirtiéndose en la primera miembro no hacker."
	},
	{
		idpersonaje:26,
		splash:'assets/images/splash-personajes/wolfrahh.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/wolfrahh.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/wolfrahh.png",
		nombrepersonaje:"Wolfrahh",
		descpersonaje:"Wolfrahh creció en un vecindario normal. Era un chico inteligente y estudioso, pero con un gran gusto por los videojuegos. Se convirtió en una estrella viral en la comunidad del streaming. A todos les encanta su estilo, pero él sigue en la búsqueda de hacer algo que complete su vida, además de los videojuegos."
	},
	{
		idpersonaje:27,
		splash:'assets/images/splash-personajes/wukong.jpg'
		imgpersonaje:"assets/images/component-personajes/normal/wukong.png",
		imgpersonaje_hover:"assets/images/component-personajes/hover/wukong.png",
		nombrepersonaje:"Wukong",
		descpersonaje:"Un mono que parece humano ¡pero amante de los plátanos!"
	}
]
$(document).ready(function(){
    $('.slide').bxSlider({
    	pager:false,
    	hideControlOnEnd:true,
		infiniteLoop:false,
		touchEnabled:false,
		adaptiveHeight:true
    });

   /*carga slide*/
		$.each(arraypersonajes, function( key, element) {
		  // console.log(key, element)
		  let current = key + 1
		  $('.p_item[data-id='+current+']').find('img.p_slide').attr('src', element.imgpersonaje)
		  $('.p_item[data-id='+current+']').find('img.p_hover').attr('src', element.imgpersonaje_hover)
		}); 
	/*fin carga slide*/
		
    /*carga array*/
     	$(".personajes_slide .p_item").click(function(){
	     	var id = $(this).data('id')
	     	arraypersonajes.forEach(function(element){
	     		if(element.idpersonaje === id){
	     			$('.personaje_img img').attr('src', element.imgpersonaje)
	     			$('.nombre_personaje').text(element.nombrepersonaje)
	     			$('.p_desc').text(element.descpersonaje)
	     			$('.personaje__view').fadeIn();
	     			$(".personaje__view").css('display','flex');
	     		}
	     	})
	 	});
	 /*fin carga vista*/
	 	// SLIDE PEDINTE
     	$('.s_prev').click(function(){
     		console.log('llego')
     		var id = $(this).data('id')
     		arraypersonajes.forEach(function(element){
     			if(element.idpersonaje =26){
     				console.log(element.idpersonaje)
	     			$('.personaje_img img').attr('src', element.imgpersonaje)
	     			$('.nombre_personaje').text(element.nombrepersonaje)
	     			$('.p_desc').text(element.descpersonaje)
	     			$('.personaje__view').fadeIn();
	     			$(".personaje__view").css('display','flex');
     				--element.idpersonaje
     				
	     		}
     		})
     	})
     	$('.s_next').click(function(){
     		console.log('llego')
     		var id = $(this).data('id')
     		arraypersonajes.forEach(function(element){
     			if(element.idpersonaje <=26){
     				console.log(element.idpersonaje)
	     			$('.personaje_img img').attr('src', element.imgpersonaje)
	     			$('.nombre_personaje').text(element.nombrepersonaje)
	     			$('.p_desc').text(element.descpersonaje)
	     			$('.personaje__view').fadeIn();
	     			$(".personaje__view").css('display','flex');
     				element.idpersonaje++
     				
	     		}
     		})
	     		
     	})
	 /*cerrar*/
	$(".personaje__view .cerrar").click(function(){
	   $(".personaje__view").fadeOut();
  	});
  	/*mostrar ranking*/
  	$(".voto_p").click(function(){
	   $(".personaje__view").hide();
	   $(".personaje__view_random").hide();
	   $(".contenedor_personajes").hide();
	   $(".ranking_personajes").show();
	   $(".ranking_personajes").css('display','flex');
  	});
  	$(".siguiente_voto").click(function(){
	   $(".ranking_personajes").hide();
	   $(".personaje__view_random").hide();
	   $(".contenedor_personajes").hide();
	   $(".seleccion__view").show();
	   $(".seleccion__view").css('display','flex');
  	});



   /*aletorio*/
  
   $(".random").click(function(){
	    var item = arraypersonajes[Math.floor(Math.random() * arraypersonajes.length)];
  		console.log(item)
  		$('.personaje_img img').attr('src', item.imgpersonaje)
 		$('.nombre_personaje').text(item.nombrepersonaje)
 		$('.p_desc').text(item.descpersonaje)
  		$('.personaje__view_random').fadeIn();
  		$(".personaje__view_random").css('display','flex');		
	});
	 /*cerrar aleatorio*/
	$(".personaje__view_random .cerrar").click(function(){
	   $(".personaje__view_random").fadeOut();
  	});



  });