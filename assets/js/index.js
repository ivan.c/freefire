(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


(lib.AnMovieClip = function(){
	this.currentSoundStreamInMovieclip;
	this.actionFrames = [];
	this.soundStreamDuration = new Map();
	this.streamSoundSymbolsList = [];

	this.gotoAndPlayForStreamSoundSync = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndPlay.call(this,positionOrLabel);
	}
	this.gotoAndPlay = function(positionOrLabel){
		this.clearAllSoundStreams();
		this.startStreamSoundsForTargetedFrame(positionOrLabel);
		cjs.MovieClip.prototype.gotoAndPlay.call(this,positionOrLabel);
	}
	this.play = function(){
		this.clearAllSoundStreams();
		this.startStreamSoundsForTargetedFrame(this.currentFrame);
		cjs.MovieClip.prototype.play.call(this);
	}
	this.gotoAndStop = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndStop.call(this,positionOrLabel);
		this.clearAllSoundStreams();
	}
	this.stop = function(){
		cjs.MovieClip.prototype.stop.call(this);
		this.clearAllSoundStreams();
	}
	this.startStreamSoundsForTargetedFrame = function(targetFrame){
		for(var index=0; index<this.streamSoundSymbolsList.length; index++){
			if(index <= targetFrame && this.streamSoundSymbolsList[index] != undefined){
				for(var i=0; i<this.streamSoundSymbolsList[index].length; i++){
					var sound = this.streamSoundSymbolsList[index][i];
					if(sound.endFrame > targetFrame){
						var targetPosition = Math.abs((((targetFrame - sound.startFrame)/lib.properties.fps) * 1000));
						var instance = playSound(sound.id);
						var remainingLoop = 0;
						if(sound.offset){
							targetPosition = targetPosition + sound.offset;
						}
						else if(sound.loop > 1){
							var loop = targetPosition /instance.duration;
							remainingLoop = Math.floor(sound.loop - loop);
							if(targetPosition == 0){ remainingLoop -= 1; }
							targetPosition = targetPosition % instance.duration;
						}
						instance.loop = remainingLoop;
						instance.position = Math.round(targetPosition);
						this.InsertIntoSoundStreamData(instance, sound.startFrame, sound.endFrame, sound.loop , sound.offset);
					}
				}
			}
		}
	}
	this.InsertIntoSoundStreamData = function(soundInstance, startIndex, endIndex, loopValue, offsetValue){ 
 		this.soundStreamDuration.set({instance:soundInstance}, {start: startIndex, end:endIndex, loop:loopValue, offset:offsetValue});
	}
	this.clearAllSoundStreams = function(){
		var keys = this.soundStreamDuration.keys();
		for(var i = 0;i<this.soundStreamDuration.size; i++){
			var key = keys.next().value;
			key.instance.stop();
		}
 		this.soundStreamDuration.clear();
		this.currentSoundStreamInMovieclip = undefined;
	}
	this.stopSoundStreams = function(currentFrame){
		if(this.soundStreamDuration.size > 0){
			var keys = this.soundStreamDuration.keys();
			for(var i = 0; i< this.soundStreamDuration.size ; i++){
				var key = keys.next().value; 
				var value = this.soundStreamDuration.get(key);
				if((value.end) == currentFrame){
					key.instance.stop();
					if(this.currentSoundStreamInMovieclip == key) { this.currentSoundStreamInMovieclip = undefined; }
					this.soundStreamDuration.delete(key);
				}
			}
		}
	}

	this.computeCurrentSoundStreamInstance = function(currentFrame){
		if(this.currentSoundStreamInMovieclip == undefined){
			if(this.soundStreamDuration.size > 0){
				var keys = this.soundStreamDuration.keys();
				var maxDuration = 0;
				for(var i=0;i<this.soundStreamDuration.size;i++){
					var key = keys.next().value;
					var value = this.soundStreamDuration.get(key);
					if(value.end > maxDuration){
						maxDuration = value.end;
						this.currentSoundStreamInMovieclip = key;
					}
				}
			}
		}
	}
	this.getDesiredFrame = function(currentFrame, calculatedDesiredFrame){
		for(var frameIndex in this.actionFrames){
			if((frameIndex > currentFrame) && (frameIndex < calculatedDesiredFrame)){
				return frameIndex;
			}
		}
		return calculatedDesiredFrame;
	}

	this.syncStreamSounds = function(){
		this.stopSoundStreams(this.currentFrame);
		this.computeCurrentSoundStreamInstance(this.currentFrame);
		if(this.currentSoundStreamInMovieclip != undefined){
			var soundInstance = this.currentSoundStreamInMovieclip.instance;
			if(soundInstance.position != 0){
				var soundValue = this.soundStreamDuration.get(this.currentSoundStreamInMovieclip);
				var soundPosition = (soundValue.offset?(soundInstance.position - soundValue.offset): soundInstance.position);
				var calculatedDesiredFrame = (soundValue.start)+((soundPosition/1000) * lib.properties.fps);
				if(soundValue.loop > 1){
					calculatedDesiredFrame +=(((((soundValue.loop - soundInstance.loop -1)*soundInstance.duration)) / 1000) * lib.properties.fps);
				}
				calculatedDesiredFrame = Math.floor(calculatedDesiredFrame);
				var deltaFrame = calculatedDesiredFrame - this.currentFrame;
				if(deltaFrame >= 2){
					this.gotoAndPlayForStreamSoundSync(this.getDesiredFrame(this.currentFrame,calculatedDesiredFrame));
				}
			}
		}
	}
}).prototype = p = new cjs.MovieClip();
// symbols:



(lib.azul_verde = function() {
	this.initialize(img.azul_verde);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,280,250);


(lib.rojo = function() {
	this.initialize(img.rojo);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,376,200);


(lib.misha = function() {
	this.initialize(img.misha);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,407,460);


(lib.azul = function() {
	this.initialize(img.azul);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,259,249);


(lib.Winterland_penguin = function() {
	this.initialize(img.Winterland_penguin);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,850,478);


(lib.kelly = function() {
	this.initialize(img.kelly);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,383,460);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Star = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Camada_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAAAiIghATIAHgnIgegaIAogEIAQgkIARAkIAoAEIgdAaIAHAng");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Star, new cjs.Rectangle(-5.6,-5.3,11.3,10.7), null);


(lib.Square = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Camada_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgvAwIAAhfIBfAAIAABfg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Square, new cjs.Rectangle(-4.8,-4.8,9.6,9.6), null);


(lib.Snow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Shape
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgfAfQgMgNAAgSQAAgRAMgOQAOgMARAAQASAAANAMQANAOAAARQAAASgNANQgNANgSAAQgRAAgOgNg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Snow, new cjs.Rectangle(-4.4,-4.4,8.9,8.9), null);


(lib.rojo_azul_g = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.azul();
	this.instance.setTransform(-44.2,-42.5,0.3414,0.3414);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-44.2,-42.5,88.4,85);


(lib.Misha = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.misha();
	this.instance.setTransform(0,0,0.8968,0.8968);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,365,412.5);


(lib.Kelly = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.kelly();
	this.instance.setTransform(0,0,0.8799,0.8799);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,337,404.8);


(lib.Container = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,0,0);


(lib.azul_g = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.azul_verde();
	this.instance.setTransform(-67.2,-60,0.48,0.48);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-67.2,-60,134.4,120);


(lib.misha_MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Misha("synched",0);
	this.instance.setTransform(182.5,226.2,1,1,0,0,0,182.5,206.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:206.2},14).to({y:226.2},15).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,365,432.5);


(lib.Kelly_MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Kelly("synched",0);
	this.instance.setTransform(168.5,202.3,1,1,0,0,0,168.5,202.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:218.3},19).to({y:202.3},20).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,337,420.8);


(lib.Globe = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Container
	this.container = new lib.Container();
	this.container.name = "container";

	this.timeline.addTween(cjs.Tween.get(this.container).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Globe, new cjs.Rectangle(0,0,0,0), null);


(lib.azul_rojo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.rojo_azul_g("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:19},14).to({y:0},15).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-44.2,-42.5,88.4,104);


(lib.azul_MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.azul_g("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:11},24).to({y:0},25).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-67.2,-60,134.4,131);


// stage content:
(lib.animacion_nieve = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.actionFrames = [0,339];
	// timeline functions:
	this.frame_0 = function() {
		this.clearAllSoundStreams();
		 
		const SNOW_NUM = 200;
		
		var container = this.globe.container;
		var snowTypes = [lib.Snow, lib.Square, lib.Star];
		var snowA = [];
		
		function start()
		{
			createSnowF();
			// Sustituimos esta linea...
			// createjs.Ticker.timingMode = createjs.Ticker.RAF;
			// Por esta otra...
			createjs.Ticker.timingMode = createjs.Ticker.framerate;
			createjs.Ticker.addEventListener("tick", animateSnowF);
			container.alpha = 0;
			createjs.Tween.get(container).to({alpha:1}, 2000);
			
		}
		
		
		// Ajustamos la opacidad (alpha) a .5
		// Y la velocidad (sp) a 3
		function createSnowF()
		{
			var snow;
			
			for (var i = 0; i < SNOW_NUM; i++)
			{
				snow = new snowTypes[Math.floor(Math.random() * snowTypes.length)]();
				snow.scaleX = snow.scaleY = 0.5 + Math.random() * 0.5;
				snow.alpha = snow.scaleX*.5;
				snow.x = stage.canvas.width * Math.random();
				snow.baseX = snow.x;
				snow.angle = 0;
				snow.angleSpeed = Math.random() * 0.05;
				snow.rangeX = 15 * Math.random();
				snow.y = stage.canvas.height * Math.random();
				snow.rot = 2 * Math.round(Math.random()) - 1;
				snow.sp = 3 + Math.random() * 1;
				container.addChild(snow);
				snowA[i] = snow;
			}
		}
		
		function animateSnowF()
		{
			for (var i = SNOW_NUM - 1; i >= 0; i--)
			{
				var snow = snowA[i];
				
				snow.rotation += 5 * snow.rot;
				snow.x = snow.baseX + Math.cos(snow.angle) * snow.rangeX;
				snow.y += snow.sp;		
				snow.angle += snow.angleSpeed;
		
				if (snow.y > stage.canvas.height / stage.scaleY + snow.nominalBounds.height)
				{
					snow.x = (stage.canvas.width / stage.scaleX) * Math.random();
					snow.y = -snow.nominalBounds.height;
				}			
			}
		}
		
		start();
	}
	this.frame_339 = function() {
		this.gotoAndPlay(2);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(339).call(this.frame_339).wait(1));

	// Nieve
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgVBZQAPgdAEgNQAGgUAAgbQAAgagHgWQgEgOgOgaIALAAIARAcIAGAPQAFAMACALIACAWQgBAagIAWQgGAOgQAbg");
	this.shape.setTransform(305.2,-52.275);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgJAxIAAgUIATAAIAAAUgAgJgcIAAgUIATAAIAAAUg");
	this.shape_1.setTransform(300.225,-52.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgeArQgJgLgCgOIAQAAQABAIAEAFQAHAHAOABQAKAAAHgFQAHgDAAgJQAAgGgGgDIgOgGIgNgDQgNgDgGgDQgLgGABgNQgBgNALgJQAKgJARAAQAWAAAKANQAHAIgBAKIgPAAQgBgFgEgFQgFgGgOgBQgJABgGADQgEADAAAHQgBAGAHAEIALAEIALADQASAEAGADQAKAGAAAOQgBANgJAKQgLAKgVAAQgVAAgJgKg");
	this.shape_2.setTransform(287.15,-52.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgEA6QgFgGAAgJIAAhBIgNAAIAAgOIANAAIAAgbIAQAAIAAAbIAQAAIAAAOIgQAAIAABAQAAAFAEACIAGABIADAAIADgBIAAAOIgGABIgHAAQgKAAgEgGg");
	this.shape_3.setTransform(279.675,-53.575);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgfAnQgNgOAAgYQAAgXANgPQANgOAUAAQAKAAAKAEQAKAGAFAHQAFAIACAKQABAHAAANIhHAAQAAAQAHAJQAGAJANAAQANAAAIgIQAEgGACgGIARAAQgBAFgEAHQgDAHgFAEQgHAHgMADQgGABgGAAQgTAAgMgNgAAcgHQgBgLgEgHQgHgMgQAAQgKAAgIAIQgHAJgBANIA2AAIAAAAg");
	this.shape_4.setTransform(271.775,-52.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgfArQgIgLgCgOIAQAAQACAIADAFQAHAHAOABQAKAAAHgFQAHgDAAgJQAAgGgGgDIgOgGIgNgDQgNgDgFgDQgLgGAAgNQAAgNAKgJQAKgJARAAQAWAAAKANQAGAIAAAKIgQAAQAAgFgEgFQgFgGgOgBQgJABgGADQgEADAAAHQgBAGAHAEIALAEIALADQASAEAGADQAKAGAAAOQAAANgLAKQgKAKgVAAQgVAAgKgKg");
	this.shape_5.setTransform(261.85,-52.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgeArQgKgLAAgOIAQAAQAAAIAEAFQAGAHAQABQAJAAAHgFQAHgDAAgJQAAgGgFgDIgPgGIgNgDQgMgDgHgDQgKgGgBgNQAAgNAKgJQALgJARAAQAWAAAKANQAHAIAAAKIgQAAQgBgFgEgFQgFgGgOgBQgKABgEADQgGADAAAHQABAGAGAEIALAEIAKADQATAEAGADQAKAGAAAOQAAANgKAKQgLAKgVAAQgVAAgJgKg");
	this.shape_6.setTransform(252.35,-52.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AglAsQgJgIAAgNQAAgOAJgHQAIgHAOgCIAagDQAGgBABgEQACgCAAgEQAAgJgGgEQgHgDgJAAQgOAAgFAHQgDAEgBAHIgQAAQABgSALgHQAMgIAPAAQAQAAALAHQALAHAAAOIAAA4IABAEQAAABABAAQAAAAABAAQAAABABAAQABAAABAAIACAAIADgBIAAANIgGABIgFAAQgJAAgEgGQgCgDgBgGQgFAHgJAFQgJAFgMAAQgNAAgJgJgAALACIgJACIgJABQgJABgEADQgIAEAAAKQgBAHAGAEQAFAEAIAAQAIAAAIgEQAOgGgBgQIAAgMIgIACg");
	this.shape_7.setTransform(242.5,-52.225);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgeArQgKgLAAgOIAPAAQACAIADAFQAHAHAOABQAKAAAHgFQAHgDAAgJQAAgGgGgDIgOgGIgNgDQgMgDgHgDQgLgGAAgNQAAgNAKgJQALgJARAAQAWAAAKANQAHAIAAAKIgQAAQgBgFgEgFQgFgGgOgBQgJABgGADQgEADAAAHQAAAGAGAEIALAEIAKADQASAEAHADQAKAGAAAOQAAANgKAKQgLAKgVAAQgVAAgJgKg");
	this.shape_8.setTransform(227.05,-52.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AghAnQgMgOAAgXQAAgYANgPQAMgPAVAAQASAAANANQAOANAAAYQAAAXgMAQQgMAQgXAAQgUAAgMgOgAgVgaQgHAMAAAPQAAAQAHALQAHAKAOAAQAQAAAGgMQAGgNABgOQAAgOgFgJQgHgNgRAAQgOAAgHALg");
	this.shape_9.setTransform(216.9,-52.225);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgEA6QgFgGAAgJIAAhBIgNAAIAAgOIANAAIAAgbIAQAAIAAAbIAQAAIAAAOIgQAAIAABAQAAAFAEACIAGABIADAAIADgBIAAAOIgGABIgHAAQgKAAgEgGg");
	this.shape_10.setTransform(209.025,-53.575);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgeArQgJgLgBgOIAPAAQABAIAEAFQAHAHAOABQAKAAAHgFQAHgDAAgJQAAgGgGgDIgOgGIgNgDQgMgDgHgDQgLgGABgNQgBgNAKgJQALgJARAAQAWAAAKANQAHAIgBAKIgPAAQgBgFgEgFQgFgGgOgBQgJABgGADQgEADAAAHQAAAGAGAEIALAEIALADQARAEAHADQAKAGAAAOQgBANgJAKQgLAKgVAAQgVAAgJgKg");
	this.shape_11.setTransform(201.75,-52.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgfAnQgNgOAAgYQAAgXANgPQANgOAUAAQAKAAAKAEQAKAGAFAHQAFAIACAKQABAHAAANIhHAAQAAAQAHAJQAGAJANAAQANAAAIgIQAEgGACgGIARAAQgBAFgEAHQgDAHgFAEQgHAHgMADQgGABgGAAQgTAAgMgNgAAcgHQgBgLgEgHQgHgMgQAAQgKAAgIAIQgHAJgBANIA2AAIAAAAg");
	this.shape_12.setTransform(191.625,-52.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AghAnQgMgOAAgXQAAgYAMgPQANgPAVAAQASAAANANQAOANAAAYQAAAXgLAQQgNAQgWAAQgVAAgMgOgAgVgaQgHAMAAAPQAAAQAHALQAHAKAOAAQAQAAAGgMQAHgNgBgOQAAgOgEgJQgHgNgRAAQgOAAgHALg");
	this.shape_13.setTransform(175.8,-52.225);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgfA4QgMgOAAgXQAAgVALgQQAMgPAUgBQAKABAIAEQAFADAFAHIAAgyIAQAAIAACHIgPAAIAAgNQgGAJgIAEQgIAEgJAAQgQAAgNgOgAgRgJQgIAJAAASQAAAQAHAKQAGAMAOgBQALABAIgLQAHgKAAgSQAAgSgHgJQgIgJgLAAQgMAAgHAKg");
	this.shape_14.setTransform(165,-53.95);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AglAsQgJgIAAgNQAAgOAIgHQAKgHANgCIAagDQAGgBACgEQABgCAAgEQAAgJgGgEQgHgDgKAAQgNAAgFAHQgDAEgBAHIgQAAQABgSALgHQAMgIAPAAQAQAAALAHQALAHAAAOIAAA4IABAEQAAABABAAQAAAAABAAQAAABABAAQABAAAAAAIADAAIADgBIAAANIgGABIgFAAQgJAAgEgGQgCgDgBgGQgFAHgKAFQgIAFgMAAQgNAAgJgJgAALACIgJACIgJABQgJABgFADQgHAEgBAKQAAAHAGAEQAFAEAHAAQAJAAAHgEQAOgGABgQIAAgMIgJACg");
	this.shape_15.setTransform(155,-52.225);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgEA6QgFgGAAgJIAAhBIgNAAIAAgOIANAAIAAgbIAQAAIAAAbIAQAAIAAAOIgQAAIAABAQAAAFAEACIAGABIADAAIADgBIAAAOIgGABIgHAAQgKAAgEgGg");
	this.shape_16.setTransform(146.825,-53.575);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgHBEIAAiHIAPAAIAACHg");
	this.shape_17.setTransform(142.2,-54.125);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AglAsQgJgIAAgNQAAgOAIgHQAKgHAOgCIAZgDQAGgBACgEQABgCAAgEQAAgJgGgEQgHgDgKAAQgNAAgFAHQgDAEgBAHIgPAAQAAgSALgHQAMgIAPAAQAQAAALAHQALAHAAAOIAAA4IABAEQAAABABAAQAAAAABAAQAAABABAAQABAAAAAAIADAAIADgBIAAANIgGABIgGAAQgIAAgEgGQgCgDgBgGQgFAHgKAFQgIAFgLAAQgPAAgIgJgAAMACIgKACIgJABQgJABgFADQgHAEgBAKQABAHAFAEQAFAEAHAAQAJAAAHgEQAPgGAAgQIAAgMIgIACg");
	this.shape_18.setTransform(135,-52.225);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgJBFIAAhVIgNAAIAAgNIANAAIAAgQQABgKADgEQAFgJAQAAIAEAAIADAAIAAAPIgDAAIgDAAQgHAAgCAEQgBAEAAAQIAQAAIAAANIgQAAIAABVg");
	this.shape_19.setTransform(126.9,-54.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AAYAzIAAg+QAAgJgDgGQgEgJgMAAIgJABQgHACgFAHQgEAEgBAGIgBAPIAAAzIgRAAIAAhjIAQAAIAAAOQAHgIAHgEQAJgEAIAAQAVAAAHAPQAFAIAAAPIAAA/g");
	this.shape_20.setTransform(113.75,-52.375);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AglAsQgJgIAAgNQAAgOAIgHQAKgHAOgCIAZgDQAGgBABgEQACgCAAgEQAAgJgGgEQgGgDgLAAQgMAAgGAHQgDAEgBAHIgPAAQAAgSALgHQALgIAQAAQAQAAALAHQALAHAAAOIAAA4IABAEQAAABABAAQAAAAABAAQAAABABAAQABAAAAAAIADAAIADgBIAAANIgGABIgGAAQgIAAgEgGQgCgDgBgGQgFAHgKAFQgIAFgLAAQgPAAgIgJgAAMACIgKACIgJABQgJABgFADQgIAEAAAKQABAHAFAEQAFAEAHAAQAJAAAHgEQAPgGAAgQIAAgMIgIACg");
	this.shape_21.setTransform(103.4,-52.225);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgKBGIAAhiIAQAAIAABigAgSgqIARgbIAUAAIgYAbg");
	this.shape_22.setTransform(95.525,-54.275);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgSBAQgEgDgFgHIAAANIgQAAIAAiIIAQAAIAAAxQAGgHAIgDQAHgEAIAAQATAAALANQAMAMAAAYQAAAYgMAPQgLAQgUAAQgLAAgIgGgAgTgLQgJAJAAASQAAAOAEAJQAHAQARAAQANAAAHgLQAHgKAAgSQAAgQgHgJQgHgKgNAAQgKAAgJAIg");
	this.shape_23.setTransform(87.625,-53.975);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AglAsQgJgIAAgNQAAgOAIgHQAKgHAOgCIAZgDQAGgBACgEQABgCAAgEQAAgJgGgEQgHgDgKAAQgNAAgFAHQgDAEgBAHIgPAAQAAgSALgHQAMgIAPAAQAQAAALAHQALAHAAAOIAAA4IABAEQAAABABAAQAAAAABAAQAAABABAAQABAAAAAAIADAAIADgBIAAANIgGABIgFAAQgJAAgEgGQgCgDgBgGQgFAHgKAFQgIAFgLAAQgOAAgJgJgAAMACIgKACIgJABQgJABgFADQgHAEgBAKQAAAHAGAEQAFAEAHAAQAJAAAHgEQAOgGABgQIAAgMIgIACg");
	this.shape_24.setTransform(77.05,-52.225);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAYBEIAAg+QgBgKgCgFQgFgJgMAAQgJAAgJAIQgJAHABATIAAA0IgRAAIAAiHIARAAIAAAyQAGgHAEgDQAIgGALAAQAWAAAHAQQAFAIAAAOIAAA/g");
	this.shape_25.setTransform(66.3,-54.125);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgfAnQgNgOAAgYQAAgXANgPQANgOAUAAQAKAAAKAEQAKAGAFAHQAFAIACAKQABAHAAANIhHAAQAAAQAHAJQAGAJANAAQANAAAIgIQAEgGACgGIARAAQgBAFgEAHQgDAHgFAEQgHAHgMADQgGABgGAAQgTAAgMgNgAAcgHQgBgLgEgHQgHgMgQAAQgKAAgIAIQgHAJgBANIA2AAIAAAAg");
	this.shape_26.setTransform(50.425,-52.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgIBEIAAh3IguAAIAAgQIBtAAIAAAQIguAAIAAB3g");
	this.shape_27.setTransform(39.475,-54.125);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgWAIIAAgQIAtAAIAAAQg");
	this.shape_28.setTransform(25.225,-52.6);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("Ag0AHIAAgOIBpgqIAAAQIhWAhIBWAhIAAARg");
	this.shape_29.setTransform(16.65,-52.125);

	this.instance = new lib.Snow();
	this.instance.setTransform(-4.45,-36.5);

	this.instance_1 = new lib.Square();
	this.instance_1.setTransform(-4.8,-72.95);

	this.instance_2 = new lib.Star();
	this.instance_2.setTransform(-25.65,-55.2);

	this.globe = new lib.Globe();
	this.globe.name = "globe";

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.globe},{t:this.instance_2},{t:this.instance_1},{t:this.instance},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(340));

	// rojo
	this.instance_3 = new lib.rojo();
	this.instance_3.setTransform(433,376,0.51,0.51);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(340));

	// Azul
	this.instance_4 = new lib.azul_MC();
	this.instance_4.setTransform(287.2,418);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(340));

	// Misha
	this.instance_5 = new lib.misha_MC();
	this.instance_5.setTransform(1033.5,271.2,1,1,0,0,0,182.5,206.2);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(9).to({_off:false},0).to({x:668.5},20,cjs.Ease.quadIn).wait(290).to({x:1026.45},20,cjs.Ease.quadOut).wait(1));

	// Kelly
	this.instance_6 = new lib.Kelly_MC();
	this.instance_6.setTransform(-149.45,275.55,1,1,0,0,0,168.5,202.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(1).to({_off:false},0).to({x:168.5,y:275.3},18,cjs.Ease.quadIn).wait(290).to({x:-150.45,y:275.55},20,cjs.Ease.quadOut).wait(11));

	// azul_rojo
	this.instance_7 = new lib.azul_rojo();
	this.instance_7.setTransform(53.2,213.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(340));

	// Back
	this.instance_8 = new lib.Winterland_penguin();
	this.instance_8.setTransform(1,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(340));

	this._renderFirstFrame();

}).prototype = p = new lib.AnMovieClip();
p.nominalBounds = new cjs.Rectangle(106.1,161.3,1109.9,336.2);
// library properties:
lib.properties = {
	id: '4E11A622958E4DABB988E0CB4AD5DC1D',
	width: 850,
	height: 478,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/azul_verde.png", id:"azul_verde"},
		{src:"images/rojo.png", id:"rojo"},
		{src:"images/misha.png", id:"misha"},
		{src:"images/azul.png", id:"azul"},
		{src:"images/Winterland_penguin.jpg", id:"Winterland_penguin"},
		{src:"images/kelly.png", id:"kelly"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['4E11A622958E4DABB988E0CB4AD5DC1D'] = {
	getStage: function() { return exportRoot.stage; },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}			
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;			
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});			
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;			
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}
an.handleSoundStreamOnTick = function(event) {
	if(!event.paused){
		var stageChild = stage.getChildAt(0);
		if(!stageChild.paused){
			stageChild.syncStreamSounds();
		}
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;